package zad1;

// ***************************************************************
//   Zakupy.java
//
//   Wykorzystuje klase Item do stworzenia przedmiot�w i dodania ich do koszyka
//   przechowywanego w ArrayList.
// ***************************************************************

import cs1.Keyboard;

public class Zakupy
{
    public static void main (String[] args)
    {
		String nazwaRzeczy;
		double cenaRzeczy;
		int ilosc;
	
		String kontynuujZakupy = "t";
		Koszyk koszyk = new Koszyk();
		do {
			System.out.print ("Podaj nazwe rzeczy: "); 
			nazwaRzeczy = Keyboard.readString();
	
			System.out.print ("Podaj cene jednostkowa: ");
			cenaRzeczy = Keyboard.readDouble();
	
			System.out.print ("Podaj ilosc: ");
			ilosc = Keyboard.readInt();
	
			koszyk.dodajDoKoszyka(nazwaRzeczy, cenaRzeczy, ilosc);
	
			System.out.print ("Zawartosc koszyka: ");
			System.out.println(koszyk.toString());
	
	
			System.out.print ("Kontynuowac zakupy (t/n)? ");
			kontynuujZakupy = Keyboard.readString();
		 } while (kontynuujZakupy.equals("t"));
	}
}

