package zad1;

// **********************************************************************
//   Koszyk.java
//
//   Reprezentuje koszyk zakupowy jako tablica przedmiotow (Items)
// **********************************************************************

import java.text.NumberFormat;

public class Koszyk
{

    private int iloscRzeczy;       // ilosc rzeczy w koszyku
    private double cenaCalkowita;  // calkowita cena przedmiotow w koszyku
    private int pojemnosc;         // biezaca pojemnosc koszyka
    private Item[] koszyk;
    // -----------------------------------------------------------
    // tworzy pusty koszyk o pojemnosci 5 przedmiotow.
    // -----------------------------------------------------------
    public Koszyk()
    {
		pojemnosc = 5;
		iloscRzeczy = 0;
		cenaCalkowita = 0.0;
		
		reallocate(pojemnosc);
    }
    
    private void reallocate(int newSize) {
    	Item[] nowyKoszyk = new Item[newSize];
    	if (koszyk != null) {
    		System.arraycopy(koszyk, 0, nowyKoszyk, 0, iloscRzeczy);
    	}
    	koszyk = nowyKoszyk;
    }

    // -------------------------------------------------------
    //  Dodaje rzecz do koszyka.
    // -------------------------------------------------------
    public void dodajDoKoszyka(String nazwaRzeczy, double cena, int ilosc)
    {
    	if (iloscRzeczy == pojemnosc) {
    		reallocate(iloscRzeczy*2);
    	}
    	koszyk[iloscRzeczy] = new Item(nazwaRzeczy, cena, ilosc);
    	iloscRzeczy++;
    	cenaCalkowita += cena*ilosc;
    }

    // -------------------------------------------------------
    //  Zwraca zawartosc koszyka wraz z podsumowaniem.
    // -------------------------------------------------------
    public String toString()
    {
		NumberFormat fmt = NumberFormat.getCurrencyInstance();
	
		String zawartosc = "\nKoszyk\n";
		zawartosc += "\nRzecz\t\tCena jednostkowa\tIlosc\tSuma\n";
	
		for (int i = 0; i < iloscRzeczy; i++)
		    zawartosc += koszyk[i].toString() + "\n";
	
		zawartosc += "\nTotal Price: " + fmt.format(cenaCalkowita);
		zawartosc += "\n";
	
		return zawartosc;
    }

    // ---------------------------------------------------------
    //  Zwieksza pojemnosc koszyka o 3
    // ---------------------------------------------------------
    private void powiekszRozmiar()
    {
    	reallocate(pojemnosc + 3);
    }

}

