package zad2i3;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Scanner;

import zad1.Item;

public final class AvailableItems {
	ArrayList<Item> items;
	
	public AvailableItems(String fileName) {
		File file = new File(fileName);
		items = new ArrayList<Item>();
		try {
			Scanner lineScanner = new Scanner(file);
			
			while(lineScanner.hasNextLine()) {
				String line = lineScanner.nextLine();
				Scanner scanner = new Scanner(line);
				scanner.useDelimiter(";");
				String nazwa = scanner.next();
				int ilosc = scanner.nextInt();
				double cena = scanner.nextDouble();
				scanner.close();

				Item item = new Item(nazwa, cena, ilosc);
				items.add(item);
			}
			
			
			lineScanner.close();
		} catch (FileNotFoundException e) {
			System.out.println("Nie znaleziono pliku z dostepnymi przedmiotami.");
		}
		
	}
	
	public String toString() {
		String zawartosc = "Rzecz\t\tCena jednostkowa\tIlosc\tSuma\n";
		for (Item item : items) {
		    zawartosc += item.toString() + "\n";
		}
		
		return zawartosc;
	}
	
	public Item getItem(String itemName, int count) {
		Item foundItem = null;
		for (Item item : items) {
			if (item.getNazwa().equalsIgnoreCase(itemName)) {
				foundItem = item;
				break;
			}
		}
		
		if (foundItem == null) {
			return null;
		}
		
		int availableCount = foundItem.getIlosc();
		if (availableCount >= count) {
			Item item = new Item(foundItem);
			foundItem.setIlosc(availableCount - count);
			item.setIlosc(count);
			return item;
		} else {
			return null;
		}
	}
}
