package zad2i3;

// ***************************************************************
//   Zakupy.java
//
//   Wykorzystuje klase Item do stworzenia przedmiot�w i dodania ich do koszyka
//   przechowywanego w ArrayList.
// ***************************************************************

import java.text.NumberFormat;
import java.util.ArrayList;

import cs1.Keyboard;
import zad1.Item;



public class Zakupy
{
	
	public static String koszykToString(ArrayList<Item> koszyk)
    {
		NumberFormat fmt = NumberFormat.getCurrencyInstance();
	
		String zawartosc = "\nKoszyk\n";
		zawartosc += "\nRzecz\t\tCena jednostkowa\tIlosc\tSuma\n";
		double cenaCalkowita = 0;
		for (Item item : koszyk) {
		    zawartosc += item.toString() + "\n";
		    cenaCalkowita += item.getCena() * item.getIlosc();
		}
	
		zawartosc += "\nTotal Price: " + fmt.format(cenaCalkowita);
		zawartosc += "\n";
	
		return zawartosc;
    }
	
    public static void main (String[] args)
    {
		String nazwaRzeczy;
		int ilosc;
	
		String kontynuujZakupy = "t";
		ArrayList<Item> koszyk = new ArrayList<Item>();
		AvailableItems availableItems = new AvailableItems("przedmioty.txt");
		do {
			System.out.println("Zawartosc magazynu:");
			System.out.println(availableItems.toString());
			System.out.print ("Podaj nazwe rzeczy: "); 
			nazwaRzeczy = Keyboard.readString();
	
			System.out.print ("Podaj ilosc: ");
			ilosc = Keyboard.readInt();
			
			Item item = availableItems.getItem(nazwaRzeczy, ilosc);
			if (item != null) {
				koszyk.add(item);
			} else {
				System.out.println("Nie znaleziono przedmiotu lub brak wystarczajacej"
						+ " liczby przedmiotow na stanie.");
			}
	
			System.out.print ("Zawartosc koszyka: ");
			System.out.println(koszykToString(koszyk));
	
	
			System.out.print ("Kontynuowac zakupy (t/n)? ");
			kontynuujZakupy = Keyboard.readString();
		 } while (kontynuujZakupy.equals("t"));
	}
}

